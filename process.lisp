;;;; -*-Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
(in-package :cl-site)

;; Copies/renders static assets (imgs, etc)
(defun process-static (static-dir output-dir)
  (let ((destination-root (merge-pathnames #P"static/" output-dir))
        (sources (remove-if #'uiop/filesystem:directory-exists-p
			    (directory (merge-pathnames "**/*.*" static-dir))))
        static-list)
    (dolist (source sources)
      (let* ((relative-path (enough-namestring source static-dir))
             (destination (merge-pathnames relative-path destination-root)))
        (when (pathname-name destination) ;; ignore directories
          (ensure-directories-exist destination)
	  ;;(format t "~a -> ~a~%" source destination)
	  (when (probe-file destination) (delete-file destination))
          (uiop:copy-file source destination)
          (push (cons :location relative-path) static-list))))
    (push (cons :static (nreverse static-list))
	         *global-context*)))

;; process-pages : Copies/renders pages - called last

(defun render-template (template &optional context output-stream)
  (let ((mustache:*load-path* (cons *PAGES-DIR* mustache:*load-path*))
        (mustache:*default-pathname-type* "html"))
    (mustache:render template context output-stream)))

(defun render-template* (template &optional context)
  (with-output-to-string (output-stream)
    (render-template template context output-stream)))

(defun render-page (content context template-path)
  (let ((output-path (make-pathname :defaults (make-path *OUTPUT-DIR* context t) :type "html")))
    (ensure-directories-exist output-path)
    (with-open-file (output-stream output-path
                                   :direction :output
                                   :if-exists :supersede)
      (with-output-to-string (string-stream)
         (let* ((stream (make-broadcast-stream output-stream string-stream))
                (page-context (append (acons :rel-path
                                              (enough-namestring (cdr (assoc :content context)) *pages-dir*)
                                              context)
                                      *global-context*))
                (page-content (render-template* content page-context)))
           (render-template template-path (acons :page-content page-content
                                                 page-context)
                            stream))))))

(def-render-hook preprocess-md-page (content context)
  (let* ((md-file (enough-namestring (make-path *pages-dir* context t) *pages-dir*)))
     (unless (string= (pathname-type md-file) "md")
       (return-from preprocess-md-page (call-next-hook content context)))
     (let ((html-file (make-pathname :defaults md-file :type "html")))
       (format t "Transforming ~A to ~A~%" md-file html-file)
       (let ((md-content (with-output-to-string
                             (out)
                           (cl-markdown:markdown content :stream out))))
         (call-next-hook md-content context)))))

(def-render-hook preprocess-lisp-page (content context)
  (let* ((lisp-file (make-path *pages-dir* context t)))
     (unless (string= (pathname-type lisp-file) "lisp")
       (return-from preprocess-lisp-page (call-next-hook content context)))
     (let ((html-file (enough-namestring (make-pathname :defaults lisp-file :type "html") *pages-dir*))
           *computed-page-content*)
       (format t "Loading ~a and creating fresh ~a...~%"
              (file-namestring lisp-file) html-file)
       ;;;###TODO not happy about loading the lisp file; it defeats the preprocessor/postprocessor purpose
       (load (merge-pathnames lisp-file *pages-dir*))
       (let ((headers (getf *computed-page-content* :headers)))
         (call-next-hook (getf *computed-page-content* :html-string)
                         (append (mapcan #'(lambda (key value)
                                             (acons key value nil))
                                         (plist-keys headers)
                                         (plist-values headers))
                                  context))))))

(defun process-page (content context)
  (let ((template-path (pathname (merge-pathnames *templates-dir* *DEFAULT-PAGE-TEMPLATE*))))
    (render-page content context template-path)))

;; Process news

(defparameter +edit-news-link+ "https://gitlab.common-lisp.net/clo/cl-site/edit/master/content/news.md")


;;
;; FLAG -- If news.html is converted to news.lisp (a la about.lisp)
;; then the following only will have to deal with generating the
;; newsbox. The news.lisp would have to include the hardcoded HTML
;; which is injected below as well.
;;
(def-render-hook generate-news (content context)
  (let ((page-path (cdr (assoc :content context)))
        (content (call-next-hook content context)))
    (when (and (string= (pathname-name page-path) "news")
               (string= (pathname-type page-path) "md"))
      (let ((newsbox (generate-news-box-content content)))
         (push (cons :newsbox newsbox) *global-context*)))
    content))

(defun shallow-copy-object (original)
  (let* ((class (class-of original))
         (copy (allocate-instance class)))
    (dolist (slot (mapcar #'c2mop:slot-definition-name (c2mop:class-slots class)))
      (when (slot-boundp original slot)
        (setf (slot-value copy slot)
              (slot-value original slot))))
    copy))

(defun generate-news-box-content (html &key (count 4))
  (let ((dom (plump-parser:parse html)))
    (let ((state :start)
          (node (plump:first-child dom))
          (news (plump:make-root))
          (current-news))
      (block done
        (flet ((next-node ()
                 (setq node (plump:next-sibling node))
                 (when (null node)
                   ;;(print "DONE")
                   (return-from done))
                 node))
          (loop
             ;;(print state)
             ;;(describe node)
             (ecase state
               (:start
                (if (and (plump:element-p node)
                         (equalp (plump:tag-name node) "html"))
                    (setq state :html
                          node (plump:first-child node))
                    ;; else
                    (next-node)))

               (:html
                (if (and (plump:element-p node)
                         (equalp (plump:tag-name node) "body"))
                    (setq state :body
                          node (plump:first-child node))
                    ;; else
                    (next-node)))

               (:body
                (if (and (plump:element-p node)
                         (equalp (plump:tag-name node) "main"))
                    (setq state :main
                          node (plump:first-child node))
                    (next-node)))

               (:main
                (if (and (plump:element-p node)
                         (equalp (plump:tag-name node) "h3"))
                    (setq state :read-news-heading)
                    (next-node)))

               (:read-news-heading
                                 
                ;; If enough news, exit
                (when (= (length (plump:children news)) count)
                  (return-from done))
                 
                ;; Start a new one
                (setq current-news
                      (plump:make-element news "section"))
                
                (let ((title-node (shallow-copy-object node)))
                  (setf (plump:tag-name title-node) "h7")
                  (plump:append-child current-news title-node))
                (next-node)
                (setq state :read-news-content))
               
               (:read-news-content
                ;; We assume the node is a paragraph and we read its content
                (if (and (plump:element-p node)
                         (equalp (plump:tag-name node) "p"))
                    (progn
                      (plump:append-child current-news (shallow-copy-object node))
                      (next-node)
                      (setq state :main))
                    ;; else
                    (next-node)))))))
      (plump:serialize news nil))))







