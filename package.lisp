(in-package :cl-user)

(defpackage #:cl-site
  (:use :cl)
  (:export #:make-site #:make-clean #:add-content #:wmd #:with-cl-who-string
	   #:*output-dir*
	   #:*paserve-port* ;; FLAG export port globals for other webservers when activated
	   ))


(defpackage #:cl-site-content
  (:use :cl :cl-site :cl-who))
