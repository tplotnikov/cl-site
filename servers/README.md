# Common-Lisp.net test webserver suite

## Background

Common Lisp Webservers have a long and storied history going back to
the beginning of the World Wide Web in the mid 1990s. CL servers and
frameworks continue to evolve rapidly today.

There is a somewhat outdated list of CL webservers
[here](https://www.cliki.net/HTTP%20server). Consider volunteering to
help bring this list up to date.
[These](https://github.com/CodyReichert/awesome-cl)
[awesome](https://github.com/azzamsa/awesome-cl-software)
[lists](https://github.com/vindarel/curated-awesome-cl) also appear
active these days.


## Purpose

This set of directories is provided as a service to clnet volunteers
as well as a simple testbed for a variety of Common Lisp HTTP servers.

Using the `serve.lisp` file in each of these directories, it should be
possible to start a local webserver which publishes the freshly built
local Common-Lisp.Net content and allows you to browse the site from
your local host.


## Support/Maintenance

Note that the Common-Lisp.Net maintainers cannot commit to maintaining
all these webservers in guaranteed working order at all times. It is
hoped that individual volunteers may step up and take responsibity for
each server (i.e. each subdirectory).

Some of these subdirectories contain only stubs (i.e. placeholders) at
present, and if it represents a server you are interested in, please
consider volunteering to fill in the missing code and maintain that
server for the CLNet community.

If there is a CL webserver which you care about or are curious about,
and it's not listed here, please feel free to add it.


## Usage

### Command Line

Use `make` followed by the desired server name. Currently existing
make targets are (note that many of these are still placeholders):

    make serve    # starts a default server (from list below) which is expected to work.
    make clack    # Aims to provide a server-agnostic veneer on top of any actual webserver.
    make cl-http  # The world's first HTTP1.1 server; powered whitehouse.gov under President Clinton.
    make htoot    # Hunchentoot (the "800lb Gorilla" of CL webservers).
    make paserve  # Legacy Portable AllegroServe (reverts to native aserve on Allegro CL).
    make woo      # The Fastest Webserver in the World.
    make zaserve  # the new "ZACL compatible" AllegroServe fork (reverts to native aserve on Allegro CL).
    

### From Lisp

Load the following files to get the corresponding servers running from
within Lisp (note: if you try to do more than one at a time, your
mileage may vary. It'd be interesting though. Who can get them all
running at the same time in the same running CL session?  (on distinct
ports, of course, as defined below)) (substitute the path to your
`cl-site/` direcotry for the `...`):

    (load ".../cl-site/servers/clack/serve")
    (load ".../cl-site/servers/cl-http/serve")
    (load ".../cl-site/servers/htoot/serve")
    (load ".../cl-site/servers/paserve/serve")
    (load ".../cl-site/servers/woo/serve")
    (load ".../cl-site/servers/zaserve/serve")



## Directory structure

(the following information will mainly be of interest if you want to
volunteer to maintain the code for one of the servers.)

Each subdirectory should contain the following:


* `package.lisp`  package definition for `:cl-site-<server>`,
    exporting `#:start-server` and `#:publish-cl-site`
    
* `cl-site-<server>.asd` should contain an ADSF system definition,
        including `:depends-on` for the server in question.

* `publish.lisp` should contain code in `:cl-site-<server>` package,
        defining the functions `publish-cl-site`, `client-test`, and
        `start-server`. See completed examples and documentation in
        placeholder code to see what these should do.

* `serve.lisp` should load the `cl-site/build.lisp` file (which loads
        core site content and renders it to output), then call the
        corresponding servers's `start-server` and `publish-cl-site`
        functions. Finally a message should be printed informing the
        user how to visit the locally served site in their browser.


Also, for each server there should be a corresponding distinct default
starting port number defined as a global parameter in
`cl-site/globals.lisp` with its symbol exported in
`cl-site/package.lisp` -- see existing one(s) for the expected
nomenclature.





